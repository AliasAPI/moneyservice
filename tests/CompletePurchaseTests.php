<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;
use AliasAPI\Money as Money;

class CompletePurchase extends TestCase
{
    private $client;
    private $pretag = '';

    public function setUp(): void
    {
        $this->markTestSkipped('Suspend testing.');

        require_once(\realpath(\dirname(__DIR__, 1).'/src/CreateClient.php'));
    }

    public function testSelectRows()
    {
        $request['actionS'] = 'select rows';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'SandboxRest';
        $request['select'] = [
            'uuid' => 'UUID',
            'cart' => 'promote-1',
            'type' => 'authorize',
            'status' => 'created',
            // payerid must already be saved server side
            'saleid' => '',
            'authorizationid' => '',
            'refundid' => ''
        ];

        $this->client = new Money\CreateClient($request);

        $response = $this->client->sendRequest();

        $body = $response['body'];

        $this->assertEquals('200', $response['status_code']);
        $this->assertEquals('OK', $response['reason']);
        $this->assertEquals($this->client->tag, $response['tag']);
        $this->assertEquals('select rows', $body['actionS']);
        $this->assertTrue(\array_key_exists('pair', $body));
        $this->assertEquals('TestClient', $body['pair']['client']);
        $this->assertEquals('SandboxRest', $body['pair']['server']);
        $this->assertArrayHasKey('rows', $body);

        if(isset($body['rows'])
           && ! empty($body['rows'])) {
               foreach($body['rows'] as $row) {
                   if(isset($row['tag'])
                      && ! empty($row['tag'])) {
                         $pretag = $row['tag'];
                         break;
                      }
               }
        }

        return $pretag;
    }

    /**
     * @depends testSelectRows
     * Do NOT remove this comment; PHPUnit needs it.
     */
    public function testCompletePurchase(string $pretag)
    {
        $request['actionS'] = 'complete purchase';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'SandboxRest';
        $request['pretag'] = $pretag;

        $client = new Money\CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'];

        $this->assertEquals('205', $response['status_code']);
        $this->assertEquals('Reset Content', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);
        $this->assertEquals('complete purchase', $body['actionS']);
        $this->assertTrue(\array_key_exists('pair', $body));
        $this->assertEquals('TestClient', $body['pair']['client']);
        $this->assertEquals('SandboxRest', $body['pair']['server']);
        $this->assertArrayHasKey('transaction', $body);

        $this->assertArrayHasKey('tag', $body['transaction']);
        $this->assertArrayHasKey('alias', $body['transaction']);
        $this->assertArrayHasKey('uuid', $body['transaction']);
        $this->assertArrayHasKey('cart', $body['transaction']);
        $this->assertArrayHasKey('type', $body['transaction']);
        $this->assertArrayHasKey('amount', $body['transaction']);
        $this->assertArrayHasKey('currency', $body['transaction']);
        $this->assertArrayHasKey('fee', $body['transaction']);
        $this->assertArrayHasKey('status', $body['transaction']);
        $this->assertArrayHasKey('updated', $body['transaction']);
        $this->assertArrayHasKey('redirect_url', $body);

        $this->assertArrayNotHasKey('transactionid', $body['transaction']);
        $this->assertArrayNotHasKey('tokenid', $body['transaction']);
        $this->assertArrayNotHasKey('payerid', $body['transaction']);
        $this->assertArrayNotHasKey('saleid', $body['transaction']);
        $this->assertArrayNotHasKey('refundid', $body['transaction']);
        $this->assertArrayNotHasKey('created', $body['transaction']);
        $this->assertArrayNotHasKey('redirect_url', $body['transaction']);

        $this->assertEquals($pretag, $body['transaction']['tag']);
        $this->assertEquals('SandboxRest', $body['transaction']['alias']);
        $this->assertEquals('UUID', $body['transaction']['uuid']);
        $this->assertEquals('promote-1', $body['transaction']['cart']);
        $this->assertEquals('authorize', $body['transaction']['type']);
        $this->assertEquals('10.00', $body['transaction']['amount']);
        $this->assertEquals('USD', $body['transaction']['currency']);
        $this->assertEquals('completed', $body['transaction']['status']);
    }

    public function tearDown(): void
    {
        unset($this->client);
    }
}
