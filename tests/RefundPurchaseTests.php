<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;
use AliasAPI\Money as Money;

class RefundPurchase extends TestCase
{
    private $client;
    private $pretag = '';

    public function setUp(): void
    {
        $this->markTestSkipped('Suspend testing.');

        require_once(\realpath(\dirname(__DIR__, 1).'/src/CreateClient.php'));
    }

    public function testSelectRows()
    {
        $request['actionS'] = 'select rows';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'SandboxRest';
        $request['select'] = [
            'uuid' => 'UUID',
            'cart' => 'promote-1',
            'type' => 'authorize',
            'status' => 'completed',
            'refundid' => ''
        ];

        $this->client = new Money\CreateClient($request);

        $response = $this->client->sendRequest();

        $body = $response['body'];

        $this->assertEquals('200', $response['status_code']);
        $this->assertEquals('OK', $response['reason']);
        $this->assertEquals($this->client->tag, $response['tag']);
        $this->assertEquals('select rows', $body['actionS']);
        $this->assertTrue(\array_key_exists('pair', $body));
        $this->assertEquals('TestClient', $body['pair']['client']);
        $this->assertEquals('SandboxRest', $body['pair']['server']);
        $this->assertArrayHasKey('rows', $body);

        if(isset($body['rows'])
           && ! empty($body['rows'])) {
               foreach($body['rows'] as $row) {
                   if(isset($row['tag'])
                      && ! empty($row['tag'])) {
                         $pretag = $row['tag'];
                         break;
                      }
               }
        }

        return $pretag;
    }

    /**
     * @depends testSelectRows
     * Do NOT remove this comment; PHPUnit needs it.
     */
    public function testRefundPurchase(string $pretag)
    {
        $request['actionS'] = 'refund purchase';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'SandboxRest';
        $request['pretag'] = $pretag;

        $this->client = new Money\CreateClient($request);

        $response = $this->client->sendRequest();

        $body = $response['body'];

        $this->assertEquals('201', $response['status_code']);
        $this->assertEquals('Created', $response['reason']);
        $this->assertEquals($this->client->tag, $response['tag']);
        $this->assertEquals('refund purchase', $body['actionS']);
        $this->assertTrue(\array_key_exists('pair', $body));
        $this->assertEquals('TestClient', $body['pair']['client']);
        $this->assertEquals('SandboxRest', $body['pair']['server']);

        $this->assertArrayHasKey('transaction', $body);
        $this->assertArrayHasKey('type', $body['transaction']);
        $this->assertArrayHasKey('amount', $body['transaction']);
        $this->assertArrayHasKey('currency', $body['transaction']);
        $this->assertArrayHasKey('fee', $body['transaction']);
        $this->assertArrayHasKey('status', $body['transaction']);

        $this->assertArrayNotHasKey('transactionid', $body['transaction']);
        $this->assertArrayNotHasKey('tokenid', $body['transaction']);
        $this->assertArrayNotHasKey('saleid', $body['transaction']);
        $this->assertArrayNotHasKey('refundid', $body['transaction']);
        $this->assertArrayNotHasKey('created', $body['transaction']);

        $this->assertEquals('refund', $body['transaction']['type']);
        $this->assertEquals('-10.00', $body['transaction']['amount']);
        $this->assertEquals('USD', $body['transaction']['currency']);
        $this->assertEquals('0.00', $body['transaction']['fee']);
        $this->assertEquals('completed', $body['transaction']['status']);
    }

    public function tearDown(): void
    {
        unset($this->client);
    }
}
