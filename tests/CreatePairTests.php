<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;
use AliasAPI\Money as Money;

class CreatePair extends TestCase
{
    private $client;

    public function setUp(): void
    {
        $this->markTestSkipped('Suspend testing.');

        require_once(\realpath(\dirname(__DIR__, 1).'/src/CreateClient.php'));
    }

    public function testShouldBeTrue()
    {
        $this->assertTrue(true);
    }

    public function testDeletePairFile()
    {
        // Delete the pair file because it is re-created in testCreatePairFile
        $request['actionS'] = 'delete pair file';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'SandboxRest';

        $this->client = new Money\CreateClient($request);

        $response = $this->client->sendRequest();

        $body = $response['body'];
        // sayd($response);
        $this->assertEquals('200', $response['status_code']);
        $this->assertEquals('OK', $response['reason']);
        $this->assertEquals($this->client->tag, $response['tag']);
        $this->assertEquals('The pair file has been deleted.', $body[0]);
    }

    public function testCreatePairFile()
    {
        $request['actionS'] = 'create pair file';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'SandboxRest';

        $this->client = new Money\CreateClient($request);

        $response = $this->client->sendRequest();

        $body = $response['body'];

        $this->assertEquals('201', $response['status_code']);
        $this->assertEquals('Created', $response['reason']);
        $this->assertEquals($this->client->tag, $response['tag']);
        $this->assertEquals('create pair file', $body['actionS']);
        $this->assertTrue(\array_key_exists('pair', $body));
        $this->assertEquals('TestClient', $body['pair']['client']);
        $this->assertEquals('SandboxRest', $body['pair']['server']);
        $this->assertEquals(
            $this->client->request['pair']['client_url'],
            $body['pair']['client_url']
        );
        $this->assertEquals(
            $this->client->request['pair']['server_url'],
            $body['pair']['server_url']
        );
        $this->assertStringContainsString('money', $body['pair']['server_url']);
        $this->assertEquals(
            $this->client->request['pair']['client_public_key'],
            $body['pair']['client_public_key']
        );
        $this->assertArrayHasKey('server_public_key', $body['pair']);
        $this->assertArrayHasKey('datetime', $body['pair']);
        $this->assertArrayHasKey('filename', $body['pair']);
        $this->assertArrayNotHasKey('shared_key', $body['pair']);

        // Send the $body as a request so that the client creates the pair file
        $client = new Money\CreateClient($body);
    }

    public function tearDown(): void
    {
        unset($this->client);
        // Delete previous result file.
        // Do not delete the files pair file here.
        // Most requests rely on the the pair files.
    }
}
