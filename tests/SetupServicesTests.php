<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;
use AliasAPI\Money as Money;

class SetupServices extends TestCase
{
    private $client;

    public function setUp(): void
    {
        $this->markTestSkipped('Suspend testing.');

        require_once(\realpath(\dirname(__DIR__, 1).'/src/CreateClient.php'));
    }

    public function testSetupServices()
    {
        $request['actionS'] = 'setup services';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'SandboxRest';
        $request['pair']['client_service'] = 'client';
        $request['pair']['server_service'] = 'money';

        $this->client = new Money\CreateClient($request);

        $response = $this->client->sendRequest();

        $body = $response['body'];

        $this->assertEquals('201', $response['status_code']);
        $this->assertEquals('Created', $response['reason']);
        $this->assertEquals($this->client->tag, $response['tag']);
        $this->assertEquals('MoneyService setup is complete.', $body[0]);
    }

    public function tearDown(): void
    {
        unset($this->client);
    }
}
