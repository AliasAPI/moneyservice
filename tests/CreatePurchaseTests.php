<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;
use AliasAPI\Money as Money;

class CreatePurchase extends TestCase
{
    private $client;

    public function setUp(): void
    {
        $this->markTestSkipped('Suspend testing.');

        require_once(\realpath(\dirname(__DIR__, 1).'/src/CreateClient.php'));
    }

    public function testCreatePurchase()
    {
        $request['actionS'] = 'create purchase';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'SandboxRest';

        $request['user']['uuid'] = 'UUID';
        $request['cart'] = 'promote-1';

        $request['order']['amount'] = '10.00';
        $request['order']['currency'] = 'USD';
        $request['order']['description'] = 'Support Service';

        $this->client = new Money\CreateClient($request);

        $response = $this->client->sendRequest();

        $body = $response['body'];

        $this->assertEquals('201', $response['status_code']);
        $this->assertEquals('Created', $response['reason']);
        $this->assertEquals($this->client->tag, $response['tag']);
        $this->assertEquals('create purchase', $body['actionS']);
        $this->assertTrue(\array_key_exists('pair', $body));
        $this->assertEquals('TestClient', $body['pair']['client']);
        $this->assertEquals('SandboxRest', $body['pair']['server']);
        $this->assertArrayHasKey('transaction', $body);

        $this->assertArrayHasKey('tag', $body['transaction']);
        $this->assertArrayHasKey('alias', $body['transaction']);
        $this->assertArrayHasKey('uuid', $body['transaction']);
        $this->assertArrayHasKey('cart', $body['transaction']);
        $this->assertArrayHasKey('type', $body['transaction']);
        $this->assertArrayHasKey('amount', $body['transaction']);
        $this->assertArrayHasKey('currency', $body['transaction']);
        $this->assertArrayHasKey('fee', $body['transaction']);
        $this->assertArrayHasKey('status', $body['transaction']);
        $this->assertArrayHasKey('updated', $body['transaction']);
        $this->assertArrayHasKey('redirect_url', $body);

        $this->assertArrayNotHasKey('transactionid', $body['transaction']);
        $this->assertArrayNotHasKey('tokenid', $body['transaction']);
        $this->assertArrayNotHasKey('payerid', $body['transaction']);
        $this->assertArrayNotHasKey('saleid', $body['transaction']);
        $this->assertArrayNotHasKey('refundid', $body['transaction']);
        $this->assertArrayNotHasKey('created', $body['transaction']);
        $this->assertArrayNotHasKey('redirect_url', $body['transaction']);

        $this->assertEquals($this->client->tag, $body['transaction']['tag']);
        $this->assertEquals('SandboxRest', $body['transaction']['alias']);
        $this->assertEquals('UUID', $body['transaction']['uuid']);
        $this->assertEquals('promote-1', $body['transaction']['cart']);
        $this->assertEquals('authorize', $body['transaction']['type']);
        $this->assertEquals('10.00', $body['transaction']['amount']);
        $this->assertEquals('USD', $body['transaction']['currency']);
        $this->assertEquals('0', $body['transaction']['fee']);
        $this->assertEquals('created', $body['transaction']['status']);

        // Display the redirect_url so that the payment can be completed at the gateway
        if (isset($body['redirect_url'])) {
            sleep(2);
            echo PHP_EOL . "Visit this link and complete the payment for the remaining tests:";
            echo PHP_EOL . $body['redirect_url'] . PHP_EOL . PHP_EOL;
            die();
        } else {
            echo PHP_EOL . "The Create Purchase test failed because there is no redirect_url.";
        }
    }

    public function tearDown(): void
    {
        unset($this->client);
    }
}
