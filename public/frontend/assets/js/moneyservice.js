$(document).ready(function(){
    function getParameterByName(name)
    {
      name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
      var regexS = "[\\?&]" + name + "=([^&#]*)";
      var regex = new RegExp(regexS);
      var results = regex.exec(window.location.search);
      if(results == null)
        return "";
      else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    var cart = getParameterByName('cart');
    var go = getParameterByName('go');
    var fullname = getParameterByName('fullname');
    var email = getParameterByName('email');
    var payerid = getParameterByName('PayerID');
    var pretag = getParameterByName('pretag');
    var tag = getParameterByName('tag');
    var uuid = getParameterByName('uuid');
    var token = getParameterByName('token');
    var paymentid = getParameterByName('paymentId');

    switch(cart) {
        case 'Starter':
            var price = 14.50;
            break;
        case 'Premium':
            var price = 21.50;
            break;
        case 'Advanced':
            var price = 42.00;
            break;
        default:
            var go = "home";
            var cart = "";
            var price = 0.00;
            break;
    }

    var query = '?';
    var query = (cart !== "") ? query.concat('&cart='+cart) : query;
    var query = (email !== "") ? query.concat('&uuid='+email) : query;
    var query = (fullname !== "") ? query.concat('&fullname='+fullname) : query;
    var query = (go !== "") ? query.concat('&go='+go) : query;
    var query = (payerid !== "") ? query.concat('&PayerID='+payerid) : query;
    var query = (paymentid !== "") ? query.concat('&paymentId='+paymentid) : query;
    var query = (pretag !== "") ? query.concat('&pretag='+pretag) : query;
    var query = (tag !== "") ? query.concat('&tag='+tag) : query;
    var query = (token !== "") ? query.concat('&token='+token) : query;
    var query = (uuid !== "") ? query.concat('&uuid='+uuid) : query;
    var query = query.replace("?&", "?");

    $("#cart").val(cart);
    $("#cart").text(cart);
    $('#price').text(price.toFixed(2));
    $('#total').text(price.toFixed(2));
    $('#link').attr('href', '../index.php'+query);
});
