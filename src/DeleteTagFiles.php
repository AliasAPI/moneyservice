<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudJson as CrudJson;

/**
 * Deletes the JSON file in jsondata folder.
 */
class DeleteTagFiles
{
    public function __invoke(Payload $payload)
    {
        try {
            CrudJson\delete_tag_files(60);

            $payload->log("Expired tag files deleted.", 4);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(501, [$ex->getMessage()]);
        }
    }
}
