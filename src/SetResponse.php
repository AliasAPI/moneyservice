<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Sets the response for the payload request.
 */
class SetResponse
{
    public function __invoke(Payload $payload)
    {
        try {
            $message = [];

            $message['actionS'] = $payload->getActionS();

            $message['pair'] = $payload->getPair();

            $transaction_row = $payload->getTransactionRow();

            if (! empty($transaction_row)) {
                $message['transaction'] = $transaction_row;
                // Remove all of the PayPal values
                unset($message['transaction']['transactionid']);
                unset($message['transaction']['tokenid']);
                unset($message['transaction']['payerid']);
                unset($message['transaction']['saleid']);
                unset($message['transaction']['refundid']);
                unset($message['transaction']['authorizationid']);
                unset($message['transaction']['created']);
                unset($message['transaction']['redirect_url']);
            }

            $rows_results = $payload->getRowsResults();

            if (! empty($rows_results)) {
                $message['rows'] = $rows_results;
            }

            $status_code = $payload->getStatusCode();

            $redirect_url = $payload->getRedirectUrl();

            if (! empty($redirect_url)) {
                $message['redirect_url'] = $redirect_url;
            }

            $payload->setMessage($message);

            $payload->setResponse();

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
