<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Sets the transaction row for inserting into the database.
 */
class SetTransactionRow
{
    private $row = [];

    public function __invoke(Payload $payload)
    {
        try {
            $trans = $payload->getTransaction();

            $row['tag'] = (isset($trans['tag'])) ? $trans['tag'] : '';

            $row['alias'] = (isset($trans['alias'])) ? $trans['alias'] : '';

            $row['uuid'] = (isset($trans['uuid'])) ? $trans['uuid'] : '';

            $row['cart'] = (isset($trans['cart'])) ? $trans['cart'] : '';

            $row['type'] = (isset($trans['type'])) ? $trans['type'] : '';

            $row['transactionid'] = (isset($trans['transactionid'])) ? $trans['transactionid'] : '';

            $row['amount'] = (isset($trans['amount'])) ? $trans['amount'] : 0;

            $row['currency'] = (isset($trans['currency'])) ? $trans['currency'] : '';

            $row['fee'] = (isset($trans['fee'])) ? $trans['fee'] : 0;

            $row['status'] = (isset($trans['status'])) ? $trans['status'] : 0;

            $row['tokenid'] = (isset($trans['tokenid'])) ? $trans['tokenid'] : '';

            $row['payerid'] = (isset($trans['payerid'])) ? $trans['payerid'] : '';

            $row['saleid'] = (isset($trans['saleid'])) ? $trans['saleid'] : '';

            $row['refundid'] = (isset($trans['refundid'])) ? $trans['refundid'] : '';

            $row['authorizationid'] = (isset($trans['authorizationid'])) ? $trans['authorizationid'] : '';

            $row['created'] = (isset($trans['created'])) ? $trans['created'] : '0000-00-00 00:00:00';

            $row['updated'] = \date('Y-m-d H:i:s', \strtotime('now'));

            $row['redirect_url'] = (isset($trans['redirect_url'])) ? $trans['redirect_url'] : '';

            $payload->setTransactionRow($row);

            $payload->log("Transaction Row Set.", 4);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
