<?php

namespace AliasAPI\Money;

use AliasAPI\Client as Client;
use AliasAPI\CrudJson as CrudJson;
use AliasAPI\CrudPair as CrudPair;
use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

/**
 * Class CreateClient
 * Autoloads files and configures a default request
 *
 * @package AliasAPI\Tests
 */
class CreateClient
{
    public $request = [];
    public $tag = '';

    /**
     * CreateClient autoloads files and sets additional default request parameters
     *
     * @param array $request
     *
     * @return void
     */
    public function __construct(array $request)
    {
        require_once(\realpath(\dirname(__DIR__, 1).'/vendor/aliasapi/frame/client/pipeline.php'));

        $this->request = Client\pipeline($request);

        $this->setTag();
    }

    public function sendRequest()
    {
        $response = Messages\request($this->request);

        return $response;
    }

    public function getPair()
    {
        $pair = CrudPair\get_pair_globals();

        return $pair;
    }

    public function setTag()
    {
        $this->tag = Messages\get_tag_global();

        return $this->tag;
    }
}
