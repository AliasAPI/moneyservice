<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Sets the redirect URL to send the user to the correct site
 */
class SetRedirectUrl
{
    public function __invoke(Payload $payload)
    {
        try {
            // actionS = create purchase | The redirect_url is set in SendPurchaseOrder
            $actionS = $payload->getActionS();

            // The following action sends the user's browser back to the Client website
            if ($actionS == 'complete purchase'
                || $actionS == 'update purchase'
                || $actionS == 'refund purchase'
            ) {
                $client_url = $payload->getClientUrl();

                $payload->setRedirectUrl($client_url);
            }

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
