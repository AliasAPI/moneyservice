<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\Money as Money;

/**
 * Creates a payload
 */
class CreatePayload
{
    public function __invoke(array $request)
    {
        // Do NOT try catch here without the payload
        $payload = new Money\Payload();

        $payload->setRequest($request);

        $payload->log("Payload Created.", 3);

        $payload->log("Sequence [ " . RUNSEQUENCE . " ] started.", 7);

        return $payload;
    }
}
