<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Sets the Update Purchase to OK
 */
class SetUpdatePurchaseOK
{
    private $update_purchase_ok = false;

    public function __invoke(Payload $payload)
    {
        try {
            $row = $payload->getTransaction();

            if ($payload->getActionS() !== 'update purchase') {
                $payload->throwError(501, ["The actionS is not set to [ update purchase ]."]);
            } elseif ($row['tag'] == '') {
                $payload->throwError(400, ["The tag is not set in the database."]);
            } elseif ($row['alias'] == '') {
                $payload->throwError(400, ["The alias is not set in the database."]);
            } elseif ($row['uuid'] == '') {
                $payload->throwError(400, ["The uuid is not set in the database."]);
            } elseif ($row['cart'] == '') {
                $payload->throwError(400, ["The cart is not set in the database."]);
            } elseif ($row['type'] !== 'authorize') {
                $payload->throwError(400, ["The type is not stored as authorize."]);
            } elseif ($row['transactionid'] == '') {
                $payload->throwError(501, ["The paymentId is not stored in the database."]);
            } elseif ($row['amount'] == '') {
                $payload->throwError(405, ["The amount is not stored in the database."]);
            } elseif ($row['status'] == 'completed') {
                $payload->throwError(405, ["The transaction status is already [ completed ]."]);
            } elseif ($row['tokenid'] == '') {
                $payload->throwError(501, ["The transaction tokenid is not stored in the database."]);
            } elseif ($row['payerid'] !== '') {
                $payload->throwError(405, ["The payerid is already stored in the database."]);
            } elseif ($row['saleid'] !== '') {
                $payload->throwError(501, ["The transaction saleid is not blank."]);
            } elseif ($row['authorizationid'] !== '') {
                $payload->throwError(501, ["The authorizationid is not blank."]);
            } elseif ($row['refundid'] !== '') {
                $payload->throwError(501, ["The transaction refundid is not blank."]);
            } else {
                $update_purchase_ok = true;

                $row['payerid'] = $payload->getPayerID();

                $payload->setTransaction($row);

                $payload->setUpdatePurchaseOK($update_purchase_ok);
            }

            $payload->log("Set Update Purchase to OK.", 5);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(424, [$ex->getMessage()]);
        }
    }
}
