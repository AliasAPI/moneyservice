<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

/**
 * Selects the transactions based on a key pairs array
 */
class SelectRows
{
    public function __invoke(Payload $payload)
    {
        try {
            $key_pairs_array = $payload->getSelect();

            // Select all of the unique transactions based on the select array
            $row = CrudTable\read_rows('transactions', $key_pairs_array, 100);

            $count = \count($row);

            if ($count == 0) {
                Messages\respond(404, ["No transactions found for the select."]);
            }

            // tour:SelectRows
            for ($i = 0; $i < $count; $i++) {
                $results[$row[$i]['tag']] = [
                    'tag' => $row[$i]['tag'],
                    'alias' => $row[$i]['alias'],
                    'uuid' =>  $row[$i]['uuid'],
                    'cart' =>  $row[$i]['cart'],
                    'amount' =>  $row[$i]['amount'],
                    'currency' =>  $row[$i]['currency'],
                    'fee' =>  $row[$i]['fee'],
                    'status' =>  $row[$i]['status'],
                    'updated' =>  $row[$i]['updated']
                ];
            }

            // Put the rows in reverse order
            if (! empty($results)) {
                \krsort($results);
            }

            $payload->log("[ " . $count . " ] transactions selected.", 3);

            $payload->setStatusCode(200);

            $payload->setRowsResults($results);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
