<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Sets the fetch transaction to OK
 */
class SetFetchTransactionOK
{
    private $fetch_transaction_ok = false;

    public function __invoke(Payload $payload)
    {
        try {
            $request = $payload->getRequest();

            $row = $payload->getTransaction();

            if ($request['actionS'] !== 'fetch transaction') {
                $payload->throwError(400, ["The actionS is not set to [ fetch transaction ]."]);
            } elseif (! isset($request['pretag'])) {
                $payload->throwError(400, ["The fetch pretag is not set in the request."]);
            } elseif (empty($row)) {
                $payload->throwError(400, ["The transaction_row is not set."]);
            } else {
                $fetch_transaction_ok = true;
            }

            $payload->setFetchTransactionOK($fetch_transaction_ok);

            $payload->log("Fetch transaction Set to OK.", 5);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
