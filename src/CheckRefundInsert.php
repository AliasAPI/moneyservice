<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudTable as CrudTable;

/**
 * Checks the refundid to see if the refund was captured correctly
 */
class CheckRefundInsert
{
    public function __invoke(Payload $payload)
    {
        try {
            $key_pairs_array = $payload->getTransactionRow();

            $row = CrudTable\read_rows('transactions', $key_pairs_array, 1);

            if (! isset($row['refundid'])
                || $row['refundid'] == ''
                || $row['refundid'] == 'FailedToCapture') {
                $payload->throwError(501, ["Use fetch transaction to retrieve the refundid."]);
            }

            $payload->log("Refundid checked.", 4);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
