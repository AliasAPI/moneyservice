<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudTable as CrudTable;

/**
 * Selects the transaction from the transactions table
 */
class SelectTransaction
{
    public function __invoke(Payload $payload)
    {
        try {
            $pretag = $payload->getPreTag();

            $key_pairs_array = [ 'tag' => $pretag ];

            // Select the unique transaction based on the transaction tag
            $transaction = CrudTable\read_rows('transactions', $key_pairs_array, 1);

            if (empty($transaction)) {
                $payload->throwError(500, ["The transaction was not found."]);
            }

            $payload->log("Tag [ " . $pretag . " ] Transaction Selected.", 3);

            $payload->setTransaction($transaction);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(424, [$ex->getMessage()]);
        }
    }
}
