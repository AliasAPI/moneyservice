<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use Omnipay\Omnipay;

/**
 * Creates a gateway to the PayPal RestGateway
 * PayPal_Rest is the one that works for the PayPal REST API
 */
class CreateGateway
{
    public function __invoke(Payload $payload)
    {
        $gateway_config = $payload->getGatewayConfig();

        try {
            // Create the gateway for the PayPal REST Gateway
            // PayPal_Rest is the one that works for the PayPal REST API
            $gateway = Omnipay::create($gateway_config['gateway']);

            $payload->setGateway($gateway);

            $payload->setStatusCode(201);

            $payload->log("Gateway Created.", 4);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
