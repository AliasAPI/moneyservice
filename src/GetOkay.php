<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Gets the OK response to determine if the pipeline process should proceed
 */
class GetOkay
{
    public function __invoke(Payload $payload)
    {
        $okay = $payload->getOkay();
        
        return $okay;
    }
}
