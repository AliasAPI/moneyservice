<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\Messages as Messages;

/**
 * Sets the action to be processed
 */
class SetSequence
{
    private $sequence = '';

    public function __invoke(array $request): string
    {
        if (isset($request['actionS'])
            && $request['actionS'] == 'create purchase') {
            $sequence = 'create purchase';
        } elseif ((isset($request['cancel'])
                  && $request['cancel'] == true)
                  || (isset($request['actionS'])
                  && $request['actionS'] == 'cancel purchase')) {
            $sequence = 'cancel purchase';
        } elseif (isset($request['actionS'])
                  && $request['actionS'] == 'complete purchase') {
            $sequence = 'complete purchase';
        } elseif (isset($request['actionS'])
                  && $request['actionS'] == 'refund purchase') {
            $sequence = 'refund purchase';
        } elseif (isset($request['actionS'])
                  && $request['actionS'] == 'update purchase') {
            $sequence = 'update purchase';
        } elseif (isset($request['actionS'])
                  && $request['actionS'] == 'fetch transaction') {
            $sequence = 'fetch transaction';
        } elseif (isset($request['actionS'])
                  && $request['actionS'] == 'select rows') {
            $sequence = 'select rows';
        } elseif (isset($request['actionS'])
                  && $request['actionS'] == 'setup services') {
            Messages\respond(201, ["MoneyService setup is complete."]);
        } else {
            Messages\respond(403, ["MoneyService cannot process the [" . $request['actionS'] . "] action"]);
        }

        \defined('RUNSEQUENCE') || \define('RUNSEQUENCE', $sequence);

        return $sequence;
    }
}
