<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Sets the select_rows_ok to true
 */
class SetSelectRowsOK
{
    public function __invoke(Payload $payload)
    {
        try {
            $request = $payload->getRequest();

            if ($request['actionS'] !== 'select rows') {
                $this->throwError(400, ["The actionS is not set to [ select rows ]."]);
            } elseif (! isset($request['select'])) {
                $this->throwError(400, ["The select array is not set in the request."]);
            } else {
                $payload->setSelectRowsOK(true);

                $payload->log("Select rows OK set to true.", 5);
            }

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
