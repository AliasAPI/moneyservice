<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudTable as CrudTable;

/**
 * Deletes the extra rows created in the `transactions` table.
 */
class DeleteExpiredRows
{
    public function __invoke(Payload $payload)
    {
        try {
            $sql = "DELETE FROM `transactions`
                    WHERE `status` = 'created'
                    AND `payerid` = ''
                    AND `saleid` = ''
                    AND `authorizationid` = ''
                    AND `refundid` = ''
                    AND `updated` <= DATE_SUB(NOW(), INTERVAL 2 HOUR)";

            CrudTable\query($sql, [], []);

            // Authorized transactions that are incomplete expire after 30 days
            $sql = "DELETE FROM `transactions`
                    WHERE `status` = 'created'
                    AND `payerid` != ''
                    AND `saleid` = ''
                    AND `authorizationid` = ''
                    AND `refundid` = ''
                    AND `updated` <= DATE_SUB(NOW(), INTERVAL 25 DAY)";

            CrudTable\query($sql, [], []);

            $payload->log("Expired rows have been deleted.", 4);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
