<?php

namespace AliasAPI\Money;

// tour:Payload
/**
 * Houses the functions for assigning values to the payload.
 */
class Payload
{
    protected $amount;
    protected $actionS;
    protected $currency;
    protected $pair;
    protected $cart;
    protected $alias_attributes = [];
    protected $create_purchase_ok = false;
    protected $complete_purchase_ok = false;
    protected $delete_purchase_ok = false;
    protected $refund_purchase_ok = false;
    protected $fetch_transaction_ok = false;
    protected $select_rows_ok = false;
    protected $setup_services_ok = false;
    protected $gateway;
    protected $gateway_config = [];
    protected $order = [];
    protected $purchase_order;
    protected $transaction = [];
    protected $database_config = [];
    protected $transaction_row = [];
    protected $log = [];
    protected $request;
    protected $paymentid;
    protected $payerid;
    protected $pretag;
    protected $client;
    protected $server;
    protected $client_url;
    protected $server_url;
    protected $tag;
    protected $redirect_url;
    protected $rows_results = [];
    protected $select = [];
    protected $writable_path;
    protected $tokenid;
    protected $saleid;
    protected $refundid;
    protected $message;
    protected $uuid;
    protected $update_purchase_ok;

    public $okay = true;
    public $status_code = 200;
    public $response = [];

    // https://codereview.stackexchange.com/questions/59051/validating-basic-data-objects

    public function setAmount(array $request)
    {
        if (isset($request['order']['amount'])) {
            $this->amount = $request['order']['amount'];
        } elseif (isset($this->request['transaction']['amount'])) {
            $this->amount = $this->request['transaction']['amount'];
        } elseif ($this->actionS == 'create purchase') {
            $this->throwError(405, ["The order amount is not set in the request."]);
        }

        return $this;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setActionS(array $request)
    {
        if (isset($request['actionS'])) {
            $this->actionS = $request['actionS'];
        } elseif (isset($request['tag_file']['actionS'])) {
            $this->actionS = $request['tag_file']['actionS'];
        } else {
            $this->throwError(405, ["The request actionS is not set."]);
        }

        // The actionS must be a pipeline sequence set in Pipeline.php
        if ($this->actionS !== 'create purchase'
            && $this->actionS !== 'complete purchase'
            && $this->actionS !== 'refund purchase'
            && $this->actionS !== 'update purchase'
            && $this->actionS !== 'fetch transaction'
            && $this->actionS !== 'select rows') {
            $this->throwError(501, ["The actionS is not set to a valid Pipeline sequence."]);
        }

        return $this;
    }

    public function getActionS()
    {
        return $this->actionS;
    }

    public function setAliasAttributes(array $request)
    {
        if (! isset($request['alias_attributes'])) {
            $this->throwError(501, ["The alias_attributes are not set."]);
        } elseif (! isset($request['alias_attributes']['authorized_actions'])) {
            $this->throwError(501, ["The authorized_actions is not set in alias_attributes."]);
        } elseif (! isset($request['alias_attributes']['database_config'])) {
            $this->throwError(501, ["The database_config is not set in alias_attributes."]);
        } elseif (! isset($request['alias_attributes']['debug_config'])) {
            $this->throwError(501, ["The debug_config is not set in alias_attributes."]);
        } elseif (! isset($request['alias_attributes']['gateway_config'])) {
            $this->throwError(501, ["The gateway_config is not set in alias_attributes."]);
        } else {
            $this->alias_attributes = $request['alias_attributes'];
        }

        return $this;
    }

    public function getAliasAttributes()
    {
        return $this->alias_attributes;
    }

    public function setCart(array $request)
    {
        if (isset($request['cart'])) {
            $this->cart = $request['cart'];
        } elseif (isset($request['transaction']['cart'])) {
            $this->cart = $request['transaction']['cart'];
        } elseif ($this->actionS == 'create purchase') {
            $this->throwError(405, ["The cart is not set in the request."]);
        }

        return $this;
    }

    public function getCart()
    {
        return $this->cart;
    }

    public function setCurrency(array $request)
    {
        if (isset($request['order']['currency'])) {
            $this->currency = $request['order']['currency'];
        } elseif (isset($this->request['transaction']['currency'])) {
            $this->currency = $this->request['transaction']['currency'];
        } elseif ($this->actionS == 'create purchase') {
            $this->throwError(405, ["The order currency is not set in the request."]);
        }

        return $this;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setClient(array $request)
    {
        if (! isset($request['pair_file']['client'])) {
            $this->throwError(405, ["The pair client is not set in the request."]);
        } else {
            $this->client = $request['pair_file']['client'];
        }

        return $this;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setClientUrl(array $request)
    {
        if (! isset($request['pair_file']['client_url'])) {
            $this->throwError(405, ["The pair client_url is not set in the request."]);
        } else {
            $this->client_url = $request['pair_file']['client_url'];
        }

        return $this;
    }

    public function getClientUrl()
    {
        return $this->client_url;
    }

    public function setDatabaseConfig(array $database_config)
    {
        if (! isset($database_config['dsn'])) {
            $this->throwError(501, ["The database_config dsn is not set."]);
        } elseif (! isset($database_config['username'])) {
            $this->throwError(501, ["The database_config username is not set."]);
        } elseif (! isset($database_config['password'])) {
            $this->throwError(501, ["The database_config password is not set."]);
        }

        $this->database_config = $database_config;

        return $this;
    }

    public function getDatabaseConfig(): array
    {
        return $this->database_config;
    }

    public function setCompletePurchaseOK(bool $complete_purchase_ok)
    {
        $this->complete_purchase_ok = $complete_purchase_ok;

        return $this;
    }

    public function getCompletePurchaseOK(): bool
    {
        return $this->complete_purchase_ok;
    }

    public function setCreatePurchaseOK(bool $create_purchase_ok)
    {
        $this->create_purchase_ok = $create_purchase_ok;

        return $this;
    }

    public function getCreatePurchaseOK(): bool
    {
        return $this->create_purchase_ok;
    }


    public function setUpdatePurchaseOK(bool $update_purchase_ok)
    {
        $this->update_purchase_ok = $update_purchase_ok;

        return $this;
    }

    public function getUpdatePurchaseOK(): bool
    {
        return $this->update_purchase_ok;
    }

    public function log(string $message, int $level)
    {
        return $this->log[] = [$message, $level];
    }

    public function getLog()
    {
        return $this->log;
    }

    public function setMessage(array $message)
    {
        $this->message = $message;

        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setPair(array $request)
    {
        if (isset($request['pair'])) {
            $this->pair = $request['pair'];
        } elseif (isset($request['tag_file']['pair'])) {
            $this->pair = $request['tag_file']['pair'];
        } else {
            $this->throwError(405, ["The request pair is not set."]);
        }

        return $this;
    }

    public function getPair()
    {
        return $this->pair;
    }

    public function setRefundPurchaseOK(bool $refund_purchase_ok)
    {
        $this->refund_purchase_ok = $refund_purchase_ok;

        return $this;
    }

    public function getRefundPurchaseOK()
    {
        return $this->refund_purchase_ok;
    }


    public function setFetchTransactionOK(bool $fetch_transaction_ok)
    {
        $this->fetch_transaction_ok = $fetch_transaction_ok;

        return $this;
    }

    public function getFetchTransactionOK(): bool
    {
        return $this->fetch_transaction_ok;
    }

    public function setSelectRowsOK(bool $select_rows_ok)
    {
        $this->select_rows_ok = $select_rows_ok;

        return $this;
    }

    public function getSelectRowsOK(): bool
    {
        return $this->select_rows_ok;
    }

    public function setSetupServicesOK(bool $setup_services_ok)
    {
        $this->$setup_services_ok = $setup_services_ok;

        return $this;
    }

    public function getSetupServicesOK(): bool
    {
        return $this->$setup_services_ok;
    }

    public function setRequest(array $request)
    {
        // Do not do any error checking here.
        // The checking is in SetParameters();
        $this->request = $request;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function setStatusCode(int $status_code)
    {
        $this->status_code = $status_code;

        return $this;
    }

    public function getStatusCode(): int
    {
        return $this->status_code;
    }

    public function throwError(int $status_code, array $message)
    {
        // Make the Pipeline InterruptibleProcessor skip the rest of the Pipeline
        $this->setOkay(false);
        $this->setStatusCode($status_code);
        $this->setMessage($message);

        throw new \ErrorException($message[0], $status_code);

        return $this;
    }

    public function setOkay(bool $okay)
    {
        $this->okay = $okay;

        return $this;
    }

    public function getOkay(): bool
    {
        return $this->okay;
    }

    public function setGateway(object $gateway)
    {
        // This is the gateway OBJECT instance
        $this->gateway = $gateway;

        return $this;
    }

    public function getGateway()
    {
        return $this->gateway;
    }

    public function setGatewayConfig(array $payload)
    {
        if (! isset($payload['alias_attributes']['gateway_config']['gateway'])
            || empty($payload['alias_attributes']['gateway_config']['gateway'])) {
            $this->throwError(501, ["The gateway_config gateway is not configured."]);
        } elseif (! isset($payload['alias_attributes']['gateway_config']['email'])
                  || empty($payload['alias_attributes']['gateway_config']['email'])) {
            $this->throwError(501, ["The gateway_config email is not configured."]);
        } elseif (! isset($payload['alias_attributes']['gateway_config']['username'])
                  || empty($payload['alias_attributes']['gateway_config']['username'])) {
            $this->throwError(501, ["The gateway_config username is not configured."]);
        } elseif (! isset($payload['alias_attributes']['gateway_config']['password'])
                  || empty($payload['alias_attributes']['gateway_config']['password'])) {
            $this->throwError(501, ["The gateway_config password is not configured."]);
        } elseif (! isset($payload['alias_attributes']['gateway_config']['testmode'])
                  || empty($payload['alias_attributes']['gateway_config']['testmode'])) {
            $this->throwError(501, ["The gateway_config testmode is not configured."]);
        } else {
            $this->gateway_config = $payload['alias_attributes']['gateway_config'];
        }

        return $this;
    }

    public function getGatewayConfig(): array
    {
        return $this->gateway_config;
    }

    public function setOrder(array $request)
    {
        if (isset($request['order'])) {
            $this->order = $request['order'];
        } elseif (isset($request['transaction'])) {
            $this->order = $request['transaction'];
        } elseif ($this->actionS == 'create purchase') {
            $this->throwError(405, ["The order is not set in the request."]);
        }

        return $this;
    }

    public function getOrder(): array
    {
        return $this->order;
    }

    public function setPurchaseOrder($purchase_order)
    {
        $this->purchase_order = $purchase_order;

        return $this;
    }

    public function getPurchaseOrder()
    {
        return $this->purchase_order;
    }

    public function setTransaction(array $transaction)
    {
        // Do not do error checking here
        $this->transaction = $transaction;

        return $this;
    }

    public function getTransaction()
    {
        return $this->transaction;
    }

    public function setTransactionRow(array $transaction)
    {
        if (! isset($transaction['tag'])) {
            $this->throwError(501, ["The transaction tag is not set."]);
        } elseif (! isset($transaction['alias'])) {
            $this->throwError(501, ["The transaction alias is not set."]);
        } elseif (! isset($transaction['uuid'])) {
            $this->throwError(501, ["The transaction uuid is not set."]);
        } elseif (! isset($transaction['cart'])) {
            $this->throwError(501, ["The transaction cart is not set."]);
        } elseif (! isset($transaction['type'])) {
            $this->throwError(501, ["The transaction type is not set."]);
        } elseif (! isset($transaction['transactionid'])) {
            $this->throwError(501, ["The transaction transactionid is not set."]);
        } elseif (! isset($transaction['amount'])) {
            $this->throwError(501, ["The transaction amount is not set."]);
        } elseif (! isset($transaction['currency'])) {
            $this->throwError(501, ["The transaction currency is not set."]);
        } elseif (! isset($transaction['fee'])) {
            $this->throwError(501, ["The transaction fee is not set."]);
        } elseif (! isset($transaction['status'])) {
            $this->throwError(501, ["The transaction status is not set."]);
        } elseif (! isset($transaction['tokenid'])) {
            $this->throwError(501, ["The transaction tokenid is not set."]);
        } elseif (! isset($transaction['payerid'])) {
            $this->throwError(501, ["The transaction payerid is not set."]);
        } elseif (! isset($transaction['saleid'])) {
            $this->throwError(501, ["The transaction saleid is not set."]);
        } elseif (! isset($transaction['refundid'])) {
            $this->throwError(501, ["The transaction refundid is not set."]);
        } elseif (! isset($transaction['created'])) {
            $this->throwError(501, ["The transaction created is not set."]);
        } elseif (! isset($transaction['updated'])) {
            $this->throwError(501, ["The transaction updated is not set."]);
        } elseif (! isset($transaction['redirect_url'])) {
            $this->throwError(501, ["The transaction redirect_url is not set."]);
        }

        $this->transaction_row = $transaction;

        return $this;
    }

    public function getTransactionRow(): array
    {
        return $this->transaction_row;
    }

    public function setRowsResults(array $results)
    {
        if (! isset($results)) {
            $this->throwError(500, ["The user results are not set."]);
        }

        $this->rows_results = $results;

        return $this;
    }

    public function getRowsResults(): array
    {
        return $this->rows_results;
    }

    public function setRedirectUrl(string $redirect_url)
    {
        $this->redirect_url = $redirect_url;

        return $this;
    }

    public function getRedirectUrl(): ?string
    {
        return $this->redirect_url;
    }

    public function setServer(array $request)
    {
        if (isset($request['pair_file']['server'])) {
            $this->server = $request['pair_file']['server'];
        } else {
            $this->throwError(405, ["The pair server is not set in the request."]);
        }

        return $this;
    }

    public function getServer(): string
    {
        return $this->server;
    }

    public function setServerUrl(array $request)
    {
        if (! isset($request['pair_file']['server_url'])) {
            $this->throwError(405, ["The pair server url is not set in the request."]);
        } else {
            $this->server_url = $request['pair_file']['server_url'];
        }

        return $this;
    }

    public function getServerUrl(): string
    {
        return $this->server_url;
    }

    public function setTag(array $request)
    {
        if (isset($request['tag'])) {
            $tag = $request['tag'];
        } elseif (isset($request['tag_file']['tag'])) {
            $tag = $request['tag_file']['tag'];
        } else {
            $this->throwError(405, ["The request tag is not set."]);
        }

        if (strlen($tag) < 3) {
            $this->throwError(400, ["The tag is too short."]);
        }

        $this->tag = $tag;

        return $this;
    }

    public function getTag(): string
    {
        return $this->tag;
    }

    public function setPaymentId(array $request)
    {
        if (isset($request['paymentId'])) {
            $this->paymentid = $request['paymentId'];
        }

        return $this;
    }

    public function getPaymentId(): string
    {
        return $this->paymentid;
    }

    public function setPayerID(array $request)
    {
        if (isset($request['PayerID'])) {
            $this->payerid = $request['PayerID'];
        }

        return $this;
    }

    public function getPayerID(): string
    {
        return $this->payerid;
    }

    public function setSaleId(array $request)
    {
        if (isset($request['saleid'])) {
            $this->saleid = $request['saleid'];
        }

        return $this;
    }

    public function getSaleId(): string
    {
        return $this->saleid;
    }

    public function setTokenId(array $request)
    {
        if (isset($request['token'])) {
            $this->tokenid = $request['token'];
        }

        return $this;
    }

    public function getTokenId(): string
    {
        return $this->tokenid;
    }

    public function setPreTag(array $request)
    {
        if (isset($request['pretag'])) {
            $this->pretag = $request['pretag'];
        } elseif (isset($request['tag_file']['transaction']['tag'])) {
            $this->pretag = $request['tag_file']['transaction']['tag'];
        } elseif (isset($request['tag'])) {
            $this->pretag = $request['tag'];
        } else {
            $this->throwError(405, ["The tag and pretag were not set in the request."]);
        }

        if (strlen($this->pretag) < 3) {
            $this->throwError(400, ["The pretag is too short."]);
        }

        return $this;
    }

    public function getPreTag(): string
    {
        return $this->pretag;
    }

    public function setRefundId(array $request)
    {
        if (isset($request['refundid'])) {
            $this->refundid = $request['refundid'];
        }

        return $this;
    }

    public function getRefundId(): string
    {
        return $this->refundid;
    }

    public function setResponse()
    {
        $this->response['status_code'] = $this->getStatusCode();

        $this->response['message'] = $this->getMessage();

        return $this;
    }

    public function getResponse(): ?array
    {
        return $this->response;
    }

    public function setUuid(array $request)
    {
        if (isset($request['user']['uuid'])) {
            $this->uuid = $request['user']['uuid'];
        } elseif (isset($request['transaction']['uuid'])) {
            $this->uuid = $request['transaction']['uuid'];
        } elseif ($this->actionS == 'create purchase') {
            $this->throwError(405, ["The request uuid is not set."]);
        }

        return $this;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setSelect(array $request)
    {
        if (isset($request['select'])
            && \is_array($request['select'])
            && ! empty($request['select'])) {
            $this->select = $request['select'];
        } elseif ($this->actionS == 'select rows') {
            $this->throwError(405, ["The select in the request is not correct."]);
        }

        return $this;
    }

    public function getSelect(): array
    {
        return $this->select;
    }

    public function setWritablePath(array $request)
    {
        if (! isset($request['writable_path'])) {
            $this->throwError(501, ["The writable_path is not set in the request."]);
        } else {
            $this->writable_path = $request['writable_path'];
        }

        return $this;
    }

    public function getWritablePath(): string
    {
        return $this->writable_path;
    }
}
