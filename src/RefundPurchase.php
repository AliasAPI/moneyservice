<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudTable as CrudTable;

/**
 * Refunds the purchase
 * Sends a refund request to the Paypal API
 */
class RefundPurchase
{
    private $row = [];

    public function __invoke(Payload $payload)
    {
        try {
            $gateway = $payload->getGateway();

            $refund_purchase_ok = $payload->getRefundPurchaseOK();

            $row = $payload->getTransaction();

            if ($refund_purchase_ok == true) {
                // tour:RefundPurchase
                $refund = $gateway->refundCapture([
                        'amount'    => $row['amount'],
                        'currency'  => $row['currency']
                    ]);

                $refund->setTransactionReference($row['saleid']);

                $reply = $refund->send();

                $data = $reply->getData();

                if (isset($data['name'])
                    && $data['name'] == 'TRANSACTION_ALREADY_REFUNDED') {
                    $key_pairs_array = [
                        'alias' => $row['alias'],
                        'cart' => $row['cart'],
                        'type' => 'refund',
                        'uuid' => $row['uuid']
                    ];

                    // Check to see if the refund transaction was previously recorded
                    $read = CrudTable\read_rows('transactions', $key_pairs_array, 1);

                    // If not, record it immediately and 'fetch transaction' later
                    if (empty($read)) {
                        $row['tag'] = $payload->getTag();
                        $row['type'] = 'refund';
                        // Store refunds as negative values to facilitate bookkeeping
                        $row['amount'] = $row['amount'] * -1;
                        $row['fee'] = 0.00;
                        $row['status'] = 'completed';
                        $row['refundid'] = 'FailedToCapture';
                    }
                } elseif ($reply->isSuccessful()) {
                    $row['tag'] = $payload->getTag();
                    $row['type'] = 'refund';

                    if (isset($data['parent_payment'])) {
                        $row['transactionid'] = $data['parent_payment'];
                    } else {
                        $payload->throwError(500, ["The gateway moved the refund paymentId."]);
                    }

                    if (isset($data['total_refunded_amount']['value'])) {
                        // Store refunds as negative values to facilitate bookkeeping
                        $row['amount'] = $data['total_refunded_amount']['value'] * -1;
                    } else {
                        $payload->throwError(500, ["The gateway moved the refunded amount."]);
                    }

                    if (isset($data['total_refunded_amount']['currency'])) {
                        $row['currency'] = $data['total_refunded_amount']['currency'];
                    } else {
                        $payload->throwError(500, ["The gateway moved the refunded currency."]);
                    }

                    // 2DO+++ Is the Sandbox server wrong in refunding transaction fees?
                    // https://paypal.github.io/PayPal-PHP-SDK/docs/class-PayPal.Api.DetailedRefund.html
                    // Returns: [refund_from_transaction_fee] => Array ( [currency] => USD [value] => 0.29 )
                    // and [refund_from_received_amount] => Array ( [currency] => USD [value] => 9.71
                    
                    $row['fee'] = 0.00;

                    if (isset($data['state'])) {
                        $row['status'] = $data['state'];
                    } else {
                        $payload->throwError(500, ["The gateway moved the purchase state."]);
                    }

                    if (isset($data['id'])) {
                        $row['refundid'] = $data['id'];
                    } else {
                        $payload->throwError(500, ["The gateway moved the refund id."]);
                    }

                    if (isset($data['create_time'])) {
                        $row['created'] = \date('Y-m-d H:i:s', \strtotime($data['create_time']));
                    } else {
                        $payload->throwError(500, ["The gateway moved the refund create_time."]);
                    }
                }

                $payload->setStatusCode(205);

                $payload->log("Tag [ " . $row['tag'] . " ] transaction refunded.", 6);

                $payload->setTransaction($row);
            }

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
