<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Captures the transaction
 * This will send a capture request through PaypalAPI.
 * Complete the payment after the user pays at the purchase at payment_gateway
 * https://github.com/thephpleague/omnipay-paypal/blob/master/src/Message/RestCompletePurchaseRequest.php
 * https://developer.paypal.com/docs/integration/direct/payments/authorize-and-capture-payments/#capture-an-authorized-payment
 */
class CapturePurchase
{
    private $row = [];

    public function __invoke(Payload $payload)
    {
        try {
            $gateway = $payload->getGateway();

            // Provided by CompletePurchase
            $row = $payload->getTransaction();

            $capture = $gateway->capture([
                    'amount'   => $row['amount'],
                    'currency' => $row['currency'],
                ]);

            $capture->setTransactionReference($row['authorizationid']);

            $reply = $capture->send();

            if ($reply->isSuccessful()) {
                // The customer has successfully paid.
                $data = $reply->getData();

                // Note: $data['intent'] is not returned in PayPal captures
                if (isset($data['transaction_fee']['value'])) {
                    $row['fee'] = $data['transaction_fee']['value'];
                } else {
                    $payload->throwError(501, ["The gateway moved the transaction_fee."]);
                }

                if (isset($data['state'])) {
                    $row['status'] = $data['state'];
                } else {
                    $payload->throwError(501, ["The gateway moved the capture state."]);
                }

                if (isset($data['id'])) {
                    $row['saleid'] = $data['id'];
                } else {
                    $payload->throwError(501, ["The gateway moved the caputure saleid."]);
                }

                if (isset($data['create_time'])) {
                    $row['created'] = \date('Y-m-d H:i:s', \strtotime($data['create_time']));
                }

                $payload->setStatusCode(201);

                $payload->log("Tag [ " . $row['tag'] . " ] Transaction Captured.", 7);

                $payload->setTransaction($row);
            } else {
                $payload->throwError(424, [$reply->getMessage()]);
            }

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
