<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Fetches the transaction.
 */
class FetchTransaction
{
    private $row = [];

    public function __invoke(Payload $payload)
    {
        try {
            $gateway = $payload->getGateway();

            $row = $payload->getTransaction();

            $fetch = $gateway->fetchPurchase();

            $fetch->setTransactionReference($row['transactionid']);

            $reply = $fetch->send();

            if ($reply->isSuccessful()) {
                $data = $reply->getData();
                says($data);
                // alias
                // uuid
                // cart

                // PayPal does not return intent=refund, so use the refund create time to emulate it
                if (isset($data['transactions'][0]['related_resources'][2]['refund']['create_time'])) {
                    $row['type'] = 'refund';
                } elseif (isset($data['intent'])) {
                    $row['type'] = $data['intent'];
                }

                if (isset($data['id'])) {
                    $row['transactionid'] = $data['id'];
                }

                if (isset($data['transactions'][0]['amount']['total'])) {
                    $row['amount'] = $data['transactions'][0]['amount']['total'];
                }

                if (isset($data['transactions'][0]['amount']['currency'])) {
                    $row['currency'] = $data['transactions'][0]['amount']['currency'];
                }

                if (isset($data['transactions'][0]['related_resources'][1]['capture']['transaction_fee']['value'])) {
                    $row['fee'] = $data['transactions'][0]['related_resources'][1]['capture']['transaction_fee']['value'];
                }

                if ($row['type'] == 'refund') {
                    $row['amount'] = $row['amount'] * -1;
                    $row['fee'] = 0.00;
                }

                if (isset($data['transactions'][0]['related_resources'][2]['refund']['state'])) {
                    $row['status'] = $data['transactions'][0]['related_resources'][2]['refund']['state'];
                } elseif (isset($data['transactions'][0]['related_resources'][1]['capture']['state'])) {
                    $row['status'] = $data['transactions'][0]['related_resources'][1]['capture']['state'];
                } elseif (isset($row['status'])) {
                    $row['status'] = $row['status'];
                } else {
                    $row['status'] = '';
                }

                if (isset($row['tokenid'])) {
                    $row['tokenid'] = $row['tokenid'];
                } else {
                    $row['tokenid'] = '';
                }

                if (isset($data['payer']['payer_info']['payer_id'])) {
                    $row['payerid'] = $data['payer']['payer_info']['payer_id'];
                } elseif (isset($row['payerid'])) {
                    $row['payerid'] = $row['payerid'];
                } else {
                    $row['payerid'] = '';
                }

                if (isset($data['transactions'][0]['related_resources'][1]['capture']['id'])) {
                    $row['saleid'] = $data['transactions'][0]['related_resources'][1]['capture']['id'];
                } elseif (isset($row['saleid'])) {
                    $row['saleid'] = $row['saleid'];
                } else {
                    $row['saleid'] = '';
                }

                if (isset($data['transactions'][0]['related_resources'][0]['authorization']['id'])) {
                    $row['authorizationid'] = $data['transactions'][0]['related_resources'][0]['authorization']['id'];
                } elseif (isset($row['authorizationid'])) {
                    $row['authorizationid'] = $row['authorizationid'];
                } else {
                    $row['authorizationid'] = '';
                }

                if (isset($data['transactions'][0]['related_resources'][2]['refund']['id'])) {
                    $row['refundid'] = $data['transactions'][0]['related_resources'][2]['refund']['id'];
                } elseif (isset($row['refundid'])) {
                    $row['refundid'] = $row['refundid'];
                } else {
                    $row['refundid'] = '';
                }

                if (isset($data['transactions'][0]['related_resources'][2]['refund']['create_time'])) {
                    $created = $data['transactions'][0]['related_resources'][2]['refund']['create_time'];
                    $row['created'] = \date('Y-m-d H:i:s', \strtotime($created));
                } elseif (isset($row['created'])) {
                    $row['created'] = $row['created'];
                } else {
                    $row['create'] = '0000-00-00 00:00:00';
                }

                $row['updated'] = \date('Y-m-d H:i:s', \strtotime('now'));

                if (isset($data['transactions'][0]['related_resources'][0]['authorization']['links'][3]['href'])) {
                    $row['redirect_url'] = $data['transactions'][0]['related_resources'][0]['authorization']['links'][3]['href'];
                }

                $payload->setStatusCode(201);

                $payload->log("Tag [ " . $row['tag'] . " ] Transaction Fetched.", 7);

                $payload->setTransaction($row);
            } else {
                $payload->throwError(424, [$reply->getMessage()]);
            }

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(501, [$ex->getMessage()]);
        }
    }
}
