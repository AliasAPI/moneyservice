<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Completes the transaction you selected.
 * This will send a complete purchase request through PaypalAPI.
 */
class CompletePurchase
{
    private $row = [];

    public function __invoke(Payload $payload)
    {
        // Complete the payment after the user pays at the purchase at the payment_gateway
        // https://github.com/thephpleague/omnipay-paypal/blob/master/src/Message/RestCompletePurchaseRequest.php
        try {
            $gateway = $payload->getGateway();

            $row = $payload->getTransaction();

            // tour:CompletePurchase
            $purchase = $gateway->completePurchase([
                    'transactionReference' => $row['transactionid'],
                    'payerId'              => $row['payerid']
                ]);

            $reply = $purchase->send();

            if ($reply->isSuccessful()) {
                // The customer has been successfully approved to pay.
                $data = $reply->getData();

                if (isset($row[0]['tag'])) {
                    $row['tag'] = $row[0]['tag'];
                } else {
                    $payload->throwError(501, ["read_rows was changed."]);
                }

                if (isset($data['id'])) {
                    $row['transactionid'] = $data['id'];
                }

                if (isset($data['intent'])) {
                    $row['type'] = $data['intent'];
                }

                if (isset($data['transactions'][0]['amount']['total'])) {
                    $row['amount'] = $data['transactions'][0]['amount']['total'];
                }

                if (isset($data['transactions'][0]['amount']['currency'])) {
                    $row['currency'] = $data['transactions'][0]['amount']['currency'];
                }

                // fee = 0
                if (isset($data['transactions'][0]['amount']['currency']['details']['fee'])) {
                    $row['fee'] = $data['transactions'][0]['amount']['currency']['details']['fee'];
                }

                // state: [authorized], completed, partially_refunded, pending, refunded, denied
                if (isset($data['transactions'][0]['related_resources'][0]['authorization']['state'])) {
                    $row['status'] = $data['transactions'][0]['related_resources'][0]['authorization']['state'];
                }

                if (isset($row[0]['tokenid'])) {
                    $row['tokenid'] = $row[0]['tokenid'];
                }

                if (isset($data['transactions'][0]['related_resources'][0]['authorization']['id'])) {
                    $row['authorizationid'] = $data['transactions'][0]['related_resources'][0]['authorization']['id'];
                } else {
                    $payload->throwError(502, ["The gateway moved the authorization id."]);
                }

                if (isset($data['payer']['payer_info']['payer_id'])) {
                    $row['payerid'] = $data['payer']['payer_info']['payer_id'];
                }

                $row['saleid'] = '';
                $row['refundid'] = '';

                if (isset($data['transactions'][0]['related_resources'][0]['authorization']['create_time'])) {
                    $create = $data['transactions'][0]['related_resources'][0]['authorization']['create_time'];
                    $row['created'] = \date('Y-m-d H:i:s', \strtotime($create));
                }

                $row['updated'] = \date('Y-m-d H:i:s', \strtotime('now'));

                $row['redirect_url'] = '';

                $payload->setStatusCode(201);

                $payload->log("Tag [ " . $row['tag'] . " ] Transaction Completed.", 7);

                $payload->setTransaction($row);
            } else {
                $payload->throwError(424, [$reply->getMessage()]);
            }

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
