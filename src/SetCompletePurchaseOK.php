<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Sets the Complete Purchase to OK
 */
class SetCompletePurchaseOK
{
    private $complete_purchase_ok = false;

    public function __invoke(Payload $payload)
    {
        try {
            $tag = $payload->getTag();

            $pretag = $payload->getPreTag();

            $gateway_config = $payload->getGatewayConfig();

            $row = $payload->getTransaction();

            if ($payload->getActionS() !== 'complete purchase') {
                $payload->throwError(501, ["The actionS is not set to [ complete purchase ]."]);
            } elseif (! isset($pretag)) {
                $payload->throwError(400, ["The complete pretag is not set in the request."]);
            } elseif ($tag == $pretag) {
                $payload->throwError(400, ["The tag and pretag are the same value."]);
            } elseif (empty($gateway_config)) {
                $payload->throwError(501, ["The gateway_config is not set."]);
            } elseif ($row['transactionid'] == '') {
                $payload->throwError(501, ["The transactionid is not recorded."]);
            } elseif ($row['payerid'] == '' && $gateway_config['gateway'] == 'PayPal_Rest') {
                $payload->throwError(501, ["The payerid is not recorded."]);
            } elseif ($row['status'] == 'completed') {
                $payload->throwError(501, ["The transaction status is already [ completed ]."]);
            } elseif ($row['saleid'] !== '') {
                $payload->throwError(501, ["The transaction saleid is not blank."]);
            } elseif ($row['refundid'] !== '') {
                $payload->throwError(501, ["The transaction refundid is not blank."]);
            } else {
                $complete_purchase_ok = true;

                $payload->log("Set Complete Purchase to OK.", 5);
            }

            $payload->setCompletePurchaseOK($complete_purchase_ok);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(424, [$ex->getMessage()]);
        }
    }
}
