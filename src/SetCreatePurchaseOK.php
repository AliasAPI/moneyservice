<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Sets the Create Purchase to OK
 */
class SetCreatePurchaseOK
{
    private $create_purchase_ok = false;

    public function __invoke(Payload $payload)
    {
        try {
            $request = $payload->getRequest();

            if ($request['actionS'] !== 'create purchase') {
                $payload->throwError(400, ["The actionS is not set to [ create purchase ]."]);
            } elseif (! isset($request['tag'])) {
                $payload->throwError(400, ["The tag is not set in the request."]);
            } else {
                $create_purchase_ok = true;

                $payload->log("Set Create Purchase to OK.", 5);
            }

            $payload->setCreatePurchaseOK($create_purchase_ok);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
