<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Initializes the gateway to the Paypal API
 */
class InitializeGateway
{
    public function __invoke(Payload $payload)
    {
        try {
            // Initialize the gateway
            $gateway = $payload->getGateway();

            $gateway_config = $payload->getGatewayConfig();

            $gateway->initialize([
                    'clientId' => $gateway_config['username'],
                    'secret'   => $gateway_config['password'],
                    'testMode' => $gateway_config['testmode']
                ]);

            $payload->setStatusCode(201);

            $payload->log("Gateway Initialized.", 4);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(424, [$ex->getMessage()]);
        }
    }
}
