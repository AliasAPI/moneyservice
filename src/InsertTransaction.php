<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudTable as CrudTable;

/**
 * Inserts the transaction to the table
 */
class InsertTransaction
{
    public function __invoke(Payload $payload)
    {
        try {
            $transaction_row = $payload->getTransactionRow();

            $row = [];

            $row['tag'] = $transaction_row['tag'];
            $row['alias'] = $transaction_row['alias'];
            $row['uuid'] = $transaction_row['uuid'];
            $row['cart'] = $transaction_row['cart'];

            $row['type'] = $transaction_row['type'];
            $row['transactionid'] = $transaction_row['transactionid'];
            $row['amount'] = $transaction_row['amount'];
            $row['currency'] = $transaction_row['currency'];
            $row['fee'] = $transaction_row['fee'];

            $row['status'] = $transaction_row['status'];
            $row['tokenid'] = $transaction_row['tokenid'];
            $row['payerid'] = $transaction_row['payerid'];
            $row['saleid'] = $transaction_row['saleid'];
            $row['authorizationid'] = $transaction_row['authorizationid'];
            $row['refundid'] = $transaction_row['refundid'];
            $row['created'] = $transaction_row['created'];
            $row['updated'] = $transaction_row['updated'];
            $row['redirect_url'] = $transaction_row['redirect_url'];

            $id = CrudTable\create_row('transactions', $row);

            $payload->setStatusCode(201);

            $payload->log("Tag [" . $row['tag'] . "] transaction recorded.", 5);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(501, [$ex->getMessage()]);
        }
    }
}
