<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\Construe as Construe;

/**
 * Builds the redirect URLS for PayPal
 */
class BuildRedirectUrls
{
    public function __invoke(Payload $payload)
    {
        try {
            $req = [];

            $request = $payload->getRequest();

            // Get the purchase details from the Payload class
            $req['order'] = $payload->getOrder();

            $server_url = $payload->getServerUrl();

            $tag = $payload->getTag();

            $cart = $payload->getCart();

            $parsed_url = Construe\parse_url($server_url);

            $confirm = [ 'params' => [ 'tag' => $tag, 'cart' => $cart ]];
            $cancel = [ 'params' => [ 'tag' => $tag ]];

            if (isset($request['go'])) {
                // If using the frontend site, add the go parameters
                $confirm = [ 'params' => [ 'tag' => $tag, 'go' => 'confirm', 'cart' => $cart ]];
                $cancel = [ 'params' => [ 'tag' => $tag, 'go' => 'cancel' ]];
            }

            $cancel_url = Construe\unparse_url($parsed_url, $cancel);
            $return_url = Construe\unparse_url($parsed_url, $confirm);

            // Add the redirect_urls to the $order
            $req['order']['cancel_url'] = $cancel_url;
            $req['order']['return_url'] = $return_url;

            $payload->setStatusCode(201);

            $payload->log("Redirect Urls Built.", 4);

            // Update the purchase details with the redirect_urls
            $payload->setOrder($req);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
