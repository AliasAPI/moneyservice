<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudTable as CrudTable;

/**
 * Creates a table on the database if there is no table created yet.
 */
class CreateTable
{
    public function __invoke(Payload $payload)
    {
        try {
            $sql = "CREATE TABLE IF NOT EXISTS `transactions` (
                      `tag` varchar(100) NOT NULL,
                      `alias` varchar(100) NOT NULL,
                      `uuid` varchar(100) NOT NULL,
                      `cart` varchar(100) NOT NULL,
                      `type` varchar(10) NOT NULL,
                      `transactionid` varchar(100) NOT NULL,
                      `amount` double(10,2) NOT NULL,
                      `currency` varchar(10) NOT NULL,
                      `fee` double(10,2) NOT NULL,
                      `status` varchar(100) NOT NULL,
                      `tokenid` varchar(100) NOT NULL,
                      `payerid` varchar(100) NOT NULL,
                      `saleid` varchar(100) NOT NULL,
                      `authorizationid` varchar(100) NOT NULL,
                      `refundid` varchar(100) NOT NULL,
                      `created` datetime NOT NULL,
                      `updated` datetime NOT NULL,
                      `redirect_url` varchar(100) NOT NULL,
                      PRIMARY KEY (`tag`),
                      KEY `uuid` (`uuid`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";

            CrudTable\query($sql);

            CrudTable\query("COMMIT;");

            $payload->log("Created Table.", 3);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(501, [$ex->getMessage()]);
        }
    }
}
