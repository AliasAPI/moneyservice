<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Sets the refund purchase to OK
 */
class SetRefundPurchaseOK
{
    private $refund_purchase_ok = false;

    public function __invoke(Payload $payload)
    {
        try {
            $request = $payload->getRequest();

            $row = $payload->getTransaction();

            if ($request['actionS'] !== 'refund purchase') {
                $payload->throwError(400, ["The actionS is not set to [ refund purchase ]."]);
            } elseif (! isset($request['tag'])) {
                $payload->throwError(400, ["The tag is not set in the request."]);
            } elseif (! isset($request['pretag'])) {
                $payload->throwError(400, ["The refund pretag is not set in the request."]);
            } elseif ($request['pretag'] == $request['tag']) {
                $payload->throwError(400, ["The tag and pretag are the same value."]);
            } elseif (! isset($row['alias'])) {
                $payload->throwError(501, ["The alias is not set in the database."]);
            } elseif (! isset($row['amount'])) {
                $payload->throwError(501, ["The amount is not set in the database."]);
            } elseif (! isset($row['currency'])) {
                $payload->throwError(501, ["The currency is not set in the database."]);
            } elseif ($row['currency'] == '') {
                $payload->throwError(501, ["The currency is not set in the database."]);
            } elseif (! isset($row['cart'])) {
                $payload->throwError(501, ["The cart is not set in the database."]);
            } elseif (! isset($row['uuid'])) {
                $payload->throwError(501, ["The uuid is not set in the database."]);
            } elseif (! isset($row['type'])) {
                $payload->throwError(501, ["The type is not set in the database."]);
            } elseif ($row['type'] !== 'authorize') {
                $payload->throwError(501, ["The transaction type is not authorize."]);
            } elseif (! isset($row['transactionid'])) {
                $payload->throwError(501, ["The transactionid is not set in the database."]);
            } elseif (! isset($row['refundid'])) {
                $payload->throwError(501, ["The refundid is not set in the database."]);
            } elseif ($row['refundid'] !== '') {
                $payload->throwError(400, ["The transaction has already been refunded."]);
            } elseif ($row['authorizationid'] == '') {
                $payload->throwError(501, ["The authorizationid is not set in the database."]);
            } elseif (! isset($row['status'])) {
                $payload->throwError(501, ["The status is not set in the database."]);
            } elseif ($row['status'] !== 'completed') {
                $payload->throwError(400, ["The transaction status is not [ completed ]."]);
            } else {
                $refund_purchase_ok = true;

                $payload->log("Refund Purchase Set to OK.", 5);
            }

            $payload->setRefundPurchaseOK($refund_purchase_ok);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
