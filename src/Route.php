<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\Money as Money;
use AliasAPI\Server as Server;
use AliasAPI\Autoload as Autoload;
use AliasAPI\Construe as Construe;
use AliasAPI\Frontend as Frontend;
use AliasAPI\Messages as Messages;

/**
 * Routes through the MoneyService
 */
class Route
{
    private $parsed_url = [];
    private $settings = [];

    public function __construct()
    {
        $this->requireFiles();

        $url = Construe\get_url();

        $this->parsed_url = Construe\parse_url($url);

        $this->displayFrontend();

        $this->processPipelines();
    }

    private function displayFrontend()
    {
        if (isset($this->parsed_url['params']['go'])) {
            $client = new Frontend\Client($this->parsed_url);
        }
    }

    private function processPipelines()
    {
        if (isset($this->parsed_url['params']['go'])) {
            return [];
        }

        $pod = Server\pipeline($this->settings);

        if (isset($pod['alias_attributes']['service'])
            && $pod['alias_attributes']['service'] == 'money') {
            $pipe = new Money\Pipeline;

            $response = $pipe($pod);

            if (isset($response['redirect_url'])) {
                Messages\redirect_browser($response['redirect_url']);
            } else {
                Messages\respond($response['status_code'], $response['message']);
            }
        }
    }

    private function requireFiles()
    {
        if (\file_exists('../vendor/aliasapi/frame/server/bootstrap.php')) {
            require_once('../vendor/aliasapi/frame/server/bootstrap.php');
            $this->settings = Server\bootstrap();
            Autoload\require_autoload_files();
            Autoload\pipeline([], [], [], 3);
        } else {
            die("[\"Please read the README.md to setup this MoneyService.\"]");
        }
    }
}
