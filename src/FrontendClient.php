<?php

namespace AliasAPI\Frontend;

use AliasAPI\Money as Money;
use AliasAPI\Server as Server;
use AliasAPI\Autoload as Autoload;
use AliasAPI\Construe as Construe;
use AliasAPI\CrudPair as CrudPair;
use AliasAPI\Messages as Messages;

class Client
{
    private $params = [];
    private $parsed_url = [];
    private $frontend_urls = [];

    public function __construct(array $parsed_url)
    {
        if (isset($parsed_url['params'])) {
            $this->params = $parsed_url['params'];
        }

        $this->parsed_url = $parsed_url;

        $this->frontend_urls = $this->setFrontendURLs();

        $this->processPayment();
    }

    public function createPurchase()
    {
        $request['actionS'] = 'create purchase';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'SandboxRest';
        $request['user']['uuid'] = $this->params['uuid'];
        $request['cart'] = $this->params['cart'];

        switch ($this->params['cart']) {
            case 'Starter':
                $request['order']['amount'] = \number_format(14.50, 2, '.', ',');
                break;
            case 'Premium':
                $request['order']['amount'] = \number_format(21.50, 2, '.', ',');
                break;
            case 'Advanced':
                $request['order']['amount'] = \number_format(42.00, 2, '.', ',');
                break;
        }

        $request['order']['currency'] = 'USD';
        $request['order']['description'] = $this->params['cart'];
        $request['go'] = 'create';

        $client = new Money\CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'];

        // Display the redirect_url so that the payment can be completed at the gateway
        if (isset($body['redirect_url'])) {
            Messages\redirect_browser($body['redirect_url']);
        } else {
            $this->redirectFrontend('failed');
        }
    }

    public function completePurchase()
    {
        $select = [
            'type' => 'authorize',
            'status' => 'created',
            'saleid' => '',
            'authorizationid' => '',
            'refundid' => '',
        ];

        $pretag = $this->requestPreTag($select);

        $request['actionS'] = 'complete purchase';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'SandboxRest';
        $request['pretag'] = $pretag;

        $client = new Money\CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'];
        if (isset($response['status_code'])
           && $response['status_code'] < 400) {
            $this->redirectFrontend('completed');
        } else {
            $this->redirectFrontend('failed');
        }
    }

    public function confirmPurchase()
    {
        $this->settings = Server\bootstrap();

        $pod = Server\pipeline($this->settings);

        if (! isset($pod['alias_attributes']['service'])
            && $pod['alias_attributes']['service'] !== 'money') {
            $this->redirectFrontend('failed');
        }

        $pipe = new Money\Pipeline;

        $response = $pipe($pod);

        $confirm_url = Construe\parse_url($this->frontend_urls['confirm']);

        // Prove that the params are on the server side based on the tag
        unset($confirm_url['params']);
        unset($confirm_url['query']);

        $changes = [
            'params' => [
                'cart' => $this->params['cart'],
                'go' => 'complete',
                'tag' => $this->params['tag']
            ]
        ];

        $confirm_url = Construe\unparse_url($confirm_url, $changes);

        Messages\redirect_browser($confirm_url);
    }

    private function processPayment()
    {
        if (! isset($this->params['go'])) {
            die('you need to go');
        }

        if ($this->params['go'] == 'create'
            && isset($this->params['cart'])
            && isset($this->params['uuid'])) {
            $this->createPurchase();
        }

        if ($this->params['go'] == 'cancel'
            && isset($this->params['tag'])) {
            $this->redirectFrontend('cancelled');
        }

        if ($this->params['go'] == 'complete') {
            $this->completePurchase();
        }

        if ($this->params['go'] == 'confirm'
            && isset($this->params['paymentId'])
            && isset($this->params['PayerID'])
            && isset($this->params['tag'])
            && isset($this->params['token'])) {
            $this->confirmPurchase();
        }

        if ($this->params['go'] == 'refund') {
            $this->refundPurchase();
        }

        if (isset($this->params['go'])
            || empty($this->params)) {
            $this->redirectFrontend('home');
        }
    }

    public function redirectFrontend($page = 'home')
    {
        if (! \array_key_exists($page, $this->frontend_urls)) {
            Messages\respond(501, ["The [ $page ] has not been built yet."]);
        }

        Messages\redirect_browser($this->frontend_urls[$page]);
    }

    public function refundPurchase()
    {
        $select = [
            'uuid' => \str_replace("%40", "@", $this->params['uuid']),
            'type' => 'authorize',
            'status' => 'completed',
            'refundid' => ''
        ];

        $pretag = $this->requestPreTag($select);

        $request['actionS'] = 'refund purchase';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'SandboxRest';
        $request['pretag'] = $pretag;

        $client = new Money\CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'];

        $this->redirectFrontend('refunded');
    }

    private function requestPreTag(array $select)
    {
        $request['actionS'] = 'select rows';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'SandboxRest';
        $request['select'] = $select;

        $pretag = '';

        $client = new Money\CreateClient($request);

        $response = $client->sendRequest();

        $body = (array) $response['body'];

        if (isset($body['rows'])
            && ! empty($body['rows'])) {
            foreach ($body['rows'] as $row) {
                if (isset($row['tag'])
                   && ! empty($row['tag'])) {
                      $pretag = $row['tag'];
                      break;
                }
            }
        } else {
            $this->redirectFrontend('failed');
        }

        return $pretag;
    }

    public function setFrontendURLs(): array
    {
        $changes = ['file' => 'frontend/index.html'];
        $home_url = Construe\unparse_url($this->parsed_url, $changes);
        Construe\check_url($home_url, true);

        $changes = ['file' => 'frontend/cancelled.html'];
        $cancelled_url = Construe\unparse_url($this->parsed_url, $changes);
        Construe\check_url($cancelled_url, true);

        $changes = ['file' => 'frontend/completed.html'];
        $completed_url = Construe\unparse_url($this->parsed_url, $changes);
        Construe\check_url($completed_url, true);

        $changes = ['file' => 'frontend/confirm.html'];
        $confirm_url = Construe\unparse_url($this->parsed_url, $changes);
        Construe\check_url($confirm_url, true);

        $changes = ['file' => 'frontend/failed.html'];
        $failed_url = Construe\unparse_url($this->parsed_url, $changes);
        Construe\check_url($failed_url, true);

        $changes = ['file' => 'frontend/refund.html'];
        $refund_url = Construe\unparse_url($this->parsed_url, $changes);
        Construe\check_url($refund_url, true);

        $changes = ['file' => 'frontend/refunded.html'];
        $refunded_url = Construe\unparse_url($this->parsed_url, $changes);
        Construe\check_url($refunded_url, true);

        $frontend_urls = [
            "home" => "$home_url",
            "cancelled" => "$cancelled_url",
            "completed" => "$completed_url",
            "confirm" => "$confirm_url",
            "failed" => "$failed_url",
            "refund" => "$refund_url",
            "refunded" => "$refunded_url"
        ];

        return $frontend_urls;
    }
}
