<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Sends a purchase order through Paypal API
 */
class SendPurchaseOrder
{
    private $row = [];

    public function __invoke(Payload $payload)
    {
        try {
            $tag = $payload->getTag();

            $purchase_order = $payload->getPurchaseOrder();

            // Transmit the purchase order to the payment gateway immediately
            $reply = $purchase_order->send();

            $payload->log("Tag [ " . $tag . " ] Purchase Sent.", 6);

            if ($reply->isSuccessful()
                || $reply->isPending()) {
                $data = $reply->getData();

                $row['tag'] = $tag;

                $row['alias'] = $payload->getServer();
                $row['uuid'] = $payload->getUuid();
                $row['cart'] = $payload->getCart();

                if (isset($data['intent'])) {
                    $row['type'] = $data['intent'];
                } else {
                    $payload->throwError(502, ["The gateway moved intent."]);
                }

                if (isset($data['id'])) {
                    $row['transactionid'] = $data['id'];
                } else {
                    $payload->throwError(502, ["The gateway moved the paymentId"]);
                }

                if (isset($data['transactions'][0]['description'])) {
                    $row['description'] = $data['transactions'][0]['description'];
                } else {
                    $payload->throwError(502, ["The gateway moved description"]);
                }

                if (isset($data['transactions'][0]['amount']['total'])) {
                    $row['amount'] = $data['transactions'][0]['amount']['total'];
                } else {
                    $payload->throwError(502, ["The gateway moved amount total."]);
                }

                if (isset($data['transactions'][0]['amount']['currency'])) {
                    $row['currency'] = $data['transactions'][0]['amount']['currency'];
                } else {
                    $payload->throwError(502, ["The gateway moved the currency type."]);
                }

                // 2DO+++ Figure out what happens to the user if isPending()
                // Try to get status = pending and use an fetch transaction request
                // PAYMENTINFO_0_PAYMENTSTATUS
                if (isset($data['state'])) {
                    $row['status'] = $data['state'];
                } else {
                    $payload->throwError(502, ["The gateway moved the order state."]);
                }

                if (isset($data['create_time'])) {
                    $datetime = new \DateTime($data['create_time'], new \DateTimeZone('UTC'));
                    $row['created'] = $datetime->format('Y-m-d H:i:s');
                } else {
                    $payload->throwError(502, ["The gateway moved the created_time."]);
                }

                if (isset($data['links'][1]['href'])) {
                    $row['redirect_url'] = $data['links'][1]['href'];
                } else {
                    $payload->throwError(502, ["The gateway moved the approval_url."]);
                }

                if (isset($row['redirect_url'])) {
                    $payload->setRedirectUrl($row['redirect_url']);
                } else {
                    $payload->throwError(502, ["The gateway moved the redirect_url."]);
                }

                if (isset($row['redirect_url'])
                    && \strpos($row['redirect_url'], 'token') !== false) {
                    $parts = ['token' => substr($row['redirect_url'], \strpos($row['redirect_url'], 'token')+6)];
                    $parts = \array_change_key_case($parts, CASE_LOWER);
                    $row['tokenid'] = (isset($parts['token'])) ? $parts['token'] : '';
                } else {
                    $row['tokenid'] = '';
                }

                $payload->setStatusCode(201);

                $payload->log("Tag [ " . $tag . " ] Purchase Successful.", 6);

                $payload->setTransaction($row);

            // 2DO+++ Review isRedirect() https://github.com/thephpleague/omnipay-paypal/issues/169
            // This may be handled in vendor/omnipay/common/src/Common/Message/AbstractResponse.php
            // https://developer.paypal.com/docs/archive/express-checkout/integration-jsv4/ec-finalize-payment/
            } elseif ($reply->isRedirect()) {
                // This redirect should automatically forward the user
                // https://omnipay.thephpleague.com/api/responses/
                $reply->redirect();
            } else {
                $payload->throwError(424, [$reply->getMessage()]);
            }

            if ($reply->isSuccessful()) {
                $payload->log("PayPal_Rest Purchase Order Successfully Sent!", 6);
            } elseif ($reply->isPending()) {
                $payload->log("Tag [ " . $tag . " ] Purchase is Pending.", 6);
            }

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex]);
        }
    }
}
