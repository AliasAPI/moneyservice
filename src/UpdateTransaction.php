<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudTable as CrudTable;

/**
 * Updates the current selected transaction.
 */
class UpdateTransaction
{
    private $affected_rows = 0;
    private $update_pairs = [];

    public function __invoke(Payload $payload)
    {
        try {
            $update_pairs = $payload->getTransactionRow();
            // Unset the tag to avoid the MySQL error 2031
            $tag = $update_pairs['tag'];
            unset($update_pairs['tag']);

            // tour:UpdateTransaction
            $affected_rows = CrudTable\update_rows(
                'transactions',
                $update_pairs,
                ['tag' => $tag]
            );

            if ($affected_rows == 0) {
                $payload->throwError(500, ["No transaction rows were updated."]);
            }

            $payload->setStatusCode(205);

            $payload->log("[ " . $affected_rows . " ] Transaction Updated.", 6);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
