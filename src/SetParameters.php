<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Sets the parameters for the payload.
 * Do NOT add try/catch in this class.
 */
class SetParameters
{
    public function __invoke(Payload $payload)
    {
        // Retrieve the request to allow overwriting values
        $request = $payload->getRequest();

        $payload->setOkay(true);

        $payload->setStatusCode(102);

        $payload->setActionS($request);

        $payload->setPair($request);

        $payload->setClient($request);

        $payload->setServer($request);

        $payload->setClientUrl($request);

        $payload->setServerUrl($request);

        $payload->setUuid($request);

        $payload->setSelect($request);

        $payload->setCart($request);

        $payload->setAliasAttributes($request);

        $payload->setGatewayConfig($request);

        $payload->setOrder($request);

        $payload->setAmount($request);

        $payload->setCurrency($request);

        $payload->setPaymentId($request);

        $payload->setTokenId($request);

        $payload->setPayerID($request);

        $payload->setRefundId($request);

        $payload->setSaleId($request);

        $payload->setPreTag($request);

        $payload->setTag($request);

        $payload->setWritablePath($request);

        $payload->log("Parameters Set.", 4);

        return $payload;
    }
}
