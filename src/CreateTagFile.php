<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use AliasAPI\CrudJson as CrudJson;

/**
 * Creates a JSON File of the transaction
 * The file is saved in jsondata folder
 */
class CreateTagFile
{
    private $request = [];

    public function __invoke(Payload $payload)
    {
        try {
            $writable_path = $payload->getWritablePath();

            // Transactions table columns
            $row = $payload->getTransactionRow();

            $request['tag'] = $row['tag'];

            if ($payload->getActionS() == 'create purchase') {
                $request['actionS'] = 'update purchase';
            } else {
                // 2DO+++ Should TAG complete purchase datetime be set for 30 days later?
                $request['actionS'] = 'complete purchase';
            }

            $request['pair'] = $payload->getPair();

            $request['transaction'] = $row;

            $exists = CrudJson\check_file_exists($writable_path, $row['tag']);

            if ($exists) {
                CrudJson\delete_json_file($writable_path, $row['tag']);
            }

            // Store the transaction details so that when the user returns, the related transaction is found.
            $result = CrudJson\create_json_file($writable_path, $row['tag'], $request);

            if ($result) {
                $payload->setStatusCode(201);

                $payload->log("Tag File Created.", 3);
            }
            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
