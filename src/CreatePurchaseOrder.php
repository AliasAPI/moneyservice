<?php

declare(strict_types=1);

namespace AliasAPI\Money;

/**
 * Creates a Purchase request.
 * This will send a transaction through the Paypal API.
 */
class CreatePurchaseOrder
{
    public function __invoke(Payload $payload)
    {
        try {
            $gateway = $payload->getGateway();

            $order = $payload->getOrder();

            // tour::CreatePurchase
            $purchase_order = $gateway->authorize([
                    'amount' =>  "" . $order['amount'] . "",
                    'currency' => "" . $order['currency'] . "",
                    'description' => "" . $order['description'] . "",
                    'cancelUrl' => "" . $order['cancel_url'] . "",
                    'returnUrl' => "" . $order['return_url'] . "",
                ]);

            $payload->setPurchaseOrder($purchase_order);

            $payload->setStatusCode(201);

            $payload->log("Created Purchase.", 7);

            return $payload;
        } catch (\Throwable $ex) {
            $payload->throwError(500, [$ex->getMessage()]);
        }
    }
}
