<?php

declare(strict_types=1);

namespace AliasAPI\Money;

use League\Pipeline as PL;
use AliasAPI\Money as Money;

/**
 * tour:Pipeline
 * This will process the request in a pipeline.
 */
class Pipeline
{
    public function __invoke(array $request)
    {
        $sequence = new Money\SetSequence();
        $run_sequence = $sequence($request);

        // tour:CreatePurchase
        if ($run_sequence == 'create purchase') {
            $pipeline = (new PL\Pipeline(new PL\InterruptibleProcessor(new GetOkay)))
                    ->pipe(new CreatePayload($request))
                    ->pipe(new SetParameters)
                    ->pipe(new CreateTable)
                    ->pipe(new SetCreatePurchaseOK)
                    ->pipe(new CreateGateway)
                    ->pipe(new InitializeGateway)
                    ->pipe(new BuildRedirectUrls)
                    ->pipe(new DeleteExpiredRows)
                    ->pipe(new DeleteTagFiles)
                    ->pipe(new CreatePurchaseOrder)
                    ->pipe(new SendPurchaseOrder)
                    ->pipe(new SetTransactionRow)
                    ->pipe(new InsertTransaction)
                    ->pipe(new CreateTagFile)
                    ->pipe(new SetRedirectUrl)
                    ->pipe(new SetResponse);
        }

        if ($run_sequence == 'update purchase') {
            $pipeline = (new PL\Pipeline(new PL\InterruptibleProcessor(new GetOkay)))
                    ->pipe(new CreatePayload($request))
                    ->pipe(new SetParameters)
                    ->pipe(new SelectTransaction)
                    ->pipe(new SetUpdatePurchaseOK)
                    ->pipe(new SetTransactionRow)
                    ->pipe(new UpdateTransaction)
                    ->pipe(new CreateTagFile)
                    ->pipe(new SetRedirectUrl)
                    ->pipe(new SetResponse);
        }

        // tour:CompletePurchase
        if ($run_sequence == 'complete purchase') {
            $pipeline = (new PL\Pipeline(new PL\InterruptibleProcessor(new GetOkay)))
                    ->pipe(new CreatePayload($request))
                    ->pipe(new SetParameters)
                    ->pipe(new SelectTransaction)
                    ->pipe(new SetCompletePurchaseOK)
                    ->pipe(new CreateGateway)
                    ->pipe(new InitializeGateway)
                    ->pipe(new CompletePurchase)
                    ->pipe(new CapturePurchase)
                    ->pipe(new SetTransactionRow)
                    ->pipe(new UpdateTransaction)
                    ->pipe(new DeleteExpiredRows)
                    ->pipe(new DeleteTagFiles)
                    ->pipe(new SetRedirectUrl)
                    ->pipe(new SetResponse);
        }

        // tour:RefundPurchase
        if ($run_sequence == 'refund purchase') {
            $pipeline = (new PL\Pipeline(new PL\InterruptibleProcessor(new GetOkay)))
                    ->pipe(new CreatePayload($request))
                    ->pipe(new SetParameters)
                    ->pipe(new SelectTransaction)
                    ->pipe(new SetRefundPurchaseOK)
                    ->pipe(new CreateGateway)
                    ->pipe(new InitializeGateway)
                    ->pipe(new RefundPurchase)
                    ->pipe(new SetTransactionRow)
                    ->pipe(new InsertTransaction)
                    ->pipe(new CheckRefundInsert)
                    ->pipe(new DeleteExpiredRows)
                    ->pipe(new DeleteTagFiles)
                    ->pipe(new SetRedirectUrl)
                    ->pipe(new SetResponse);
        }

        // tour:FetchTransaction
        if ($run_sequence == 'fetch transaction') {
            $pipeline = (new PL\Pipeline(new PL\InterruptibleProcessor(new GetOkay)))
                    ->pipe(new CreatePayload($request))
                    ->pipe(new SetParameters)
                    ->pipe(new SelectTransaction)
                    ->pipe(new SetFetchTransactionOK)
                    ->pipe(new CreateGateway)
                    ->pipe(new InitializeGateway)
                    ->pipe(new FetchTransaction)
                    ->pipe(new SetTransactionRow)
                    ->pipe(new UpdateTransaction)
                    ->pipe(new DeleteExpiredRows)
                    ->pipe(new DeleteTagFiles)
                    ->pipe(new SetResponse);
        }

        // tour:SelectRows
        if ($run_sequence == 'select rows') {
            $pipeline = (new PL\Pipeline(new PL\InterruptibleProcessor(new GetOkay)))
                    ->pipe(new CreatePayload($request))
                    ->pipe(new SetParameters)
                    ->pipe(new DeleteExpiredRows)
                    ->pipe(new SetSelectRowsOK)
                    ->pipe(new DeleteTagFiles)
                    ->pipe(new SelectRows)
                    ->pipe(new SetResponse);
        }

        try {
            $payload = $pipeline->process($request);

            return ['status_code' => $payload->getStatusCode(), 'message' => $payload->getMessage()];
        } catch (\Exception $ex) {
            return ['status_code' => $ex->getCode(), 'message' => [$ex->getMessage()]];
        }
    }
}
