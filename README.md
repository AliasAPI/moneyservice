

Why authorize vs purchase? reduce chargebacks 

How to install the MoneyService
  Run these on your terminal (do not include the $ symbol):
$ php composer.phar install or $ composer install
$ chmod 770 ./money/jsondata
$ chmod 770 ./money/config
$ vendor/phpunit/phpunit/phpunit --debug --stderr --bootstrap ./vendor/autoload.php

DO NOT FORGET to replace the bracketed text in /config/.alias.json with the appropriate values
DO NOT FORGET to replace the bracketed text in /config/.services.json with the appropriate values
DO NOT FORGET to replace the bracketed text in /config/.frontend_urls.json with the appropriate values

Add this to authorized actions inside .alias.config:
"create purchase",
"update purchase",
"complete purchase",
"refund purchase",
"fetch transaction",
"select rows"

And change service to "money"

If you get any errors, double check your configuration in .alias.json, .services.json, and .frontend_urls.json  
